package services

import (
	"VirtualWalletGo/dao"
	"VirtualWalletGo/models"
	"context"
	"fmt"
	"github.com/cloudinary/cloudinary-go/api/uploader"
	"github.com/dgrijalva/jwt-go"
	"github.com/gorilla/mux"
	"golang.org/x/crypto/bcrypt"
	"gorm.io/gorm"
	"html/template"
	"log"
	"net/http"
	"strconv"
	"strings"
	"time"
)

var expiresAt = time.Now().Add(time.Minute * 20)

func (a *App) index(w http.ResponseWriter, r *http.Request) error {
	err := a.ParsedTemplates.ExecuteTemplate(w, "index.html", nil)
	if err != nil {
		return err
	}
	return nil
}

func (a *App) registerUser(w http.ResponseWriter, r *http.Request) error {
	if r.Method == http.MethodGet {
		err := a.ParsedTemplates.ExecuteTemplate(w, "signup-3.html", nil)
		if err != nil {
			return err
		}
	} else if r.Method == http.MethodPost {
		err := r.ParseForm()
		if err != nil {
			return err
		}
		email := r.PostForm.Get("emailAddress")
		firstName := r.PostForm.Get("firstName")
		lastName := r.PostForm.Get("lastName")
		phone := r.PostForm.Get("phone")
		password := r.PostForm.Get("loginPassword")

		user := models.User{
			Model:     gorm.Model{},
			FirstName: firstName,
			LastName:  lastName,
			Phone:     phone,
			Email:     email,
			Password:  "",
			Picture:   "",
			Dob:       time.Time{},
			HasCard:   false,
			CardInfo:  nil,
			Wallets:   nil,
		}

		if pw, err := bcrypt.GenerateFromPassword([]byte(password), 5); err == nil {
			user.Password = string(pw)
		}
		err = a.Repo.RegisterUser(&user)
		if err != nil {
			return NewHTTPError(err, 400, "Couldnt register user ;(")
		}
		_, err = a.Repo.CreateWallet(user.ID, "BGN")
		if err != nil {
			return err
		}
		log.Println("GREAT SUCCEeESS!!")
		http.Redirect(w, r, " http://localhost:8080/login", http.StatusFound)
	}
	return nil
}

func (a *App) login(w http.ResponseWriter, r *http.Request) error {
	if r.Method == http.MethodGet {
		err := a.ParsedTemplates.ExecuteTemplate(w, "login.html", nil)
		if err != nil {
			return err
		}
	} else if r.Method == http.MethodPost {
		err := r.ParseForm()
		if err != nil {
			return err
		}
		email := r.Form.Get("emailAddress")
		password := r.Form.Get("loginPassword")
		if email == "" || password == "" {
			return err
		}
		user, err := a.Repo.GetUserByMail(email)
		if err != nil {
			return NewHTTPError(err, 404, "No user found with email : "+email)
		}

		err = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(password))
		if err != nil && err == bcrypt.ErrMismatchedHashAndPassword { //Password does not match!
			return NewHTTPError(err, 400, "Invalid login credentials. Please try again")
		}

		claims := &models.UserToken{
			UserId: user.ID,
			Name:   user.FirstName + user.LastName,
			Email:  user.Email,
			StandardClaims: jwt.StandardClaims{
				ExpiresAt: expiresAt.Unix(),
				Issuer:    "testM",
			},
		}

		token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
		tokenString, err := token.SignedString([]byte(SECRET))
		if err != nil {
			return err
		}
		redirectTarget := "/auth/transactions"
		if strings.HasPrefix(tokenString, "REVEL_FLASH=;") {
			tokenString = tokenString[len("REVEL_FLASH=;")+1:]
		}
		http.SetCookie(w, &http.Cookie{
			Name:    "jwtTokenCookie",
			Value:   tokenString,
			Expires: expiresAt,
		})
		http.Redirect(w, r, redirectTarget, http.StatusFound)
	}
	return nil
}

func (a *App) logout(w http.ResponseWriter, r *http.Request) error {
	http.SetCookie(w, &http.Cookie{})
	log.Println("Logging out....")
	err := a.ParsedTemplates.ExecuteTemplate(w, "about-us.html", nil)
	if err != nil {
		return err
	}
	return nil
}

func (a *App) checkPhone(w http.ResponseWriter, r *http.Request) error {
	params := mux.Vars(r)
	phone := params["email"]
	log.Println("Gonna check if phone " + phone + " exists")
	_, err := a.Repo.GetUserByPhone(phone)
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
	}
	w.WriteHeader(http.StatusOK)
	return err
}

func (a *App) loadSend(w http.ResponseWriter, r *http.Request) error {
	if r.Method == http.MethodGet {
		token := ParseJwtToken(r)
		wallets := a.Repo.GetUserWallets(token.UserId)
		html := "<option name=\"\" value=\"\" selected disabled hidden>Select Wallet</option>"
		for _, wallet := range wallets {
			html += " <option  name=\"" + wallet.Currency + "\" = value=\"" + strconv.Itoa(int(wallet.ID)) + "\">" + wallet.Currency + ":" + strconv.Itoa(int(wallet.Balance)) + "</option>"
		}
		err := a.ParsedTemplates.ExecuteTemplate(w, "send-money.html", models.HtmlOptions{
			Html: template.HTML(html),
		})
		if err != nil {
			return err
		}
	} else if r.Method == http.MethodPost {
		err := r.ParseForm()
		if err != nil {
			return err
		}
		sendAmount := r.PostForm.Get("sendAmount")
		selectWalletId := r.PostForm.Get("selectWallet")
		phone := r.PostForm.Get("phone")
		parsedId, err := strconv.ParseUint(selectWalletId, 10, 32)
		if err != nil {
			return err
		}
		senderWallet, err := a.Repo.GetWalletById(uint(parsedId))
		parsedAmount, err := strconv.ParseFloat(sendAmount, 32)
		if senderWallet.Balance < parsedAmount {
			w.Write([]byte("Not enough balance!"))
			return NewHTTPError(err, 400, "Not enough balance!")
		}
		user, err := a.Repo.GetUserByPhone(phone)
		wallets := a.Repo.GetUserWallets(user.ID)
		if err != nil {
			return err
		}
		var receiverWallet = &models.Wallet{}
		found := false
		for _, wallet := range wallets {
			if wallet.Currency == senderWallet.Currency {
				receiverWallet = &wallet
				found = true
				break
			}
		}
		if !found {
			log.Println("Wallet in currency " + senderWallet.Currency + " was not found, creating new one")
			receiverWallet, err = a.Repo.CreateWallet(user.ID, senderWallet.Currency)
			if err != nil {
				return err
			}
		}
		tx, err := a.Repo.CreateTransaction(senderWallet, receiverWallet, parsedAmount, parsedAmount, models.Send)
		if err != nil {
			log.Println("Failed to create transaction!")
			return err
		}
		if parsedAmount >= 10000 {
			sendVerifEmail(senderWallet.Owner.Email, sendAmount, tx.ID)
			data := models.SuccessTemplate{
				Action:     "Send Money",
				ActionVerb: "sent ",
				Amount:     senderWallet.Currency + " " + sendAmount,
			}
			err = a.ParsedTemplates.ExecuteTemplate(w, "deposit-money-pending.html", data)
			return err
		}
		data := models.SuccessTemplate{
			Action:     "Send Money",
			ActionVerb: "sent ",
			Amount:     senderWallet.Currency + " " + sendAmount,
		}
		err = a.ParsedTemplates.ExecuteTemplate(w, "deposit-money-success.html", data)
	}
	return nil
}

func (a *App) deposit(w http.ResponseWriter, r *http.Request) error {
	token := ParseJwtToken(r)
	cards := a.Repo.GetUserCards(token.UserId)
	html := ""
	for _, card := range cards {
		html += " <option value=\"" + strconv.Itoa(int(card.ID)) + "\">" + card.Number + "</option>"
	}
	err := a.ParsedTemplates.ExecuteTemplate(w, "deposit-money.html", models.HtmlOptions{
		Html: template.HTML(html),
	})
	if err != nil {
		return err
	}
	return nil
}

func (a *App) postDeposit(w http.ResponseWriter, r *http.Request) error {
	token := ParseJwtToken(r)
	err := r.ParseForm()
	currency := r.Form.Get("currency")
	amount := r.Form.Get("amount")
	parsedAmount, err := strconv.ParseFloat(amount, 32)
	if err != nil {
		return err
	}
	wallet, err := a.Repo.GetUserWalletByCurrency(token.UserId, currency)
	if wallet == nil {
		log.Println("User has no wallet with currency " + currency + " , going to create new wallet and deposit to it")
		wallet, err = a.Repo.CreateWallet(token.UserId, currency)
		if err != nil {
			return err
		}
	}
	user, err := a.Repo.GetUserById(token.UserId)
	wallet.Owner = *user //to get names
	_, err = a.Repo.CreateTransaction(nil, wallet, parsedAmount, parsedAmount, models.Deposit)
	if err != nil {
		log.Println("Failed to create transaction!")
	}
	data := models.SuccessTemplate{
		Action:     "Deposit Money",
		ActionVerb: "deposited ",
		Amount:     currency + " " + amount,
	}
	err = a.ParsedTemplates.ExecuteTemplate(w, "deposit-money-success.html", data)
	if err != nil {
		return err
	}
	return nil
}

func (a *App) uploadPic(w http.ResponseWriter, r *http.Request) error {
	token := ParseJwtToken(r)
	err := r.ParseMultipartForm(32 << 20)
	if err != nil {
		return err
	}
	file, _, err := r.FormFile("picture") // Retrieve the file from form data
	if err != nil {
		return err
	}
	defer file.Close() // Close the file when we finish
	ctx := context.Background()
	resp, err := a.Cloudinary.Upload.Upload(ctx, file,
		uploader.UploadParams{Eager: "w_150,c_scale"})
	if err != nil {
		return err
	}
	user, err := a.Repo.GetUserById(token.UserId)
	if err != nil {
		return err
	}
	user.Picture = resp.Eager[0].URL
	err = a.Repo.SaveUser(user)
	if err != nil {
		return err
	}
	a.indexUser(int(user.ID), user)
	http.Redirect(w, r, "/auth/profile", http.StatusFound)
	return nil
}

func (a *App) changeUserDetails(w http.ResponseWriter, r *http.Request) error {
	token := ParseJwtToken(r)
	_, user, err := generateUserData(a.Repo, token.UserId)
	if err != nil {
		return err
	}
	err = r.ParseForm()
	if err != nil {
		return err
	}
	if r.PostForm.Get("firstName") != "" {
		user.FirstName = r.PostForm.Get("firstName")
	}
	if r.PostForm.Get("lastName") != "" {
		user.LastName = r.PostForm.Get("lastName")
	}
	if r.PostForm.Get("dob") != "" {
		dob := r.PostForm.Get("dob")
		parsedDob, err := time.Parse("01-02-2006", dob)
		if err != nil {
			return err
		}
		user.Dob = parsedDob
	}
	if r.PostForm.Get("email") != "" {
		user.Email = r.PostForm.Get("email")
	}
	if r.PostForm.Get("phone") != "" {
		user.Phone = r.PostForm.Get("phone")
	}
	currPass := r.PostForm.Get("curPass")
	if currPass != "" && currPass != user.Password {
		err = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(r.PostForm.Get("curPass")))
		if err != nil && err == bcrypt.ErrMismatchedHashAndPassword { //Password does not match!
			respondWithError(w, http.StatusUnauthorized, "Invalid login credentials. Please try again")
		}
		if r.PostForm.Get("newPass") != r.PostForm.Get("confirmPass") {
			respondWithError(w, http.StatusBadRequest, "Passwords do not match!")
		}
		if pw, err := bcrypt.GenerateFromPassword([]byte(r.PostForm.Get("newPass")), 5); err == nil {
			user.Password = string(pw)
		}
	}
	err = a.Repo.SaveUser(user)
	a.indexUser(int(user.ID), user)
	if err != nil {
		return err
	}
	http.Redirect(w, r, "/auth/profile", http.StatusFound)
	if err != nil {
		return err
	}
	return nil
}

func (a *App) searchMail(w http.ResponseWriter, r *http.Request) error {
	params := mux.Vars(r)
	searchInput := params["s"]
	results := a.emailSearch(searchInput)
	respondWithJSON(w, 200, results)
	return nil
}

func (a *App) searchName(w http.ResponseWriter, r *http.Request) error {
	params := mux.Vars(r)
	searchInput := params["s"]
	results := a.nameSearch(searchInput)
	respondWithJSON(w, 200, results)
	return nil
}

func (a *App) profile(w http.ResponseWriter, r *http.Request) error {
	token := ParseJwtToken(r)
	model, _, err := generateUserData(a.Repo, token.UserId)
	if err != nil {
		return err
	}
	err = a.ParsedTemplates.ExecuteTemplate(w, "settings-profile.html", model)
	if err != nil {
		return err
	}
	return nil
}

func generateUserData(repo *dao.RepoGormMysql, id uint) (models.UserDashboard, *models.User, error) {
	user, err := repo.GetUserById(id)
	if err != nil {
		return models.UserDashboard{}, nil, err
	}
	pic := user.Picture
	if pic == "" {
		pic = "images/resizegopher.jpg" //default
	}
	balance := sumBalance(user.Wallets)
	model := models.UserDashboard{
		Name:      user.FirstName + " " + user.LastName,
		Balance:   fmt.Sprintf("%.2f", balance),
		Dob:       user.Dob.Format("01-02-2006"),
		Email:     user.Email,
		Phone:     user.Phone,
		CardAdded: false,
		BankAdded: false,
		Picture:   pic,
		RecentTx:  nil,
	}
	return model, user, err
}
