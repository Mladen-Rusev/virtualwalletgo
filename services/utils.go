package services

import (
	"VirtualWalletGo/models"
	"encoding/json"
	"gorm.io/gorm"
	"log"
	"net/http"
)

type RepoGormMysql struct {
	db *gorm.DB
}

func respondWithError(w http.ResponseWriter, code int, message string) {
	respondWithJSON(w, code, map[string]string{"error": message})
}

func respondWithJSON(w http.ResponseWriter, code int, payload interface{}) {
	response, err := json.Marshal(payload)
	if err != nil {
		//An error occurred processing the json
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("An error occured internally"))
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write(response)
}

func ParseJwtToken(r *http.Request) *models.UserToken {
	token, ok := r.Context().Value("UserToken").(*models.UserToken)
	if !ok {
		log.Println("Failed parsing UserToken from Context ¯\\_(ツ)_/¯")
	}
	return token
}
