package services

import (
	"VirtualWalletGo/models"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"html/template"
	"log"
	"net/http"
	"reflect"
	"strconv"
	"strings"
	"time"
)

var (
	exchangeRates = make(map[string]float64, 35)
)

func LaunchExchangeRatesService() {
	log.Println("Starting exchange rates service...")
	for {
		log.Println("Calling getExchangeRates")
		go getExchangeRates()
		time.Sleep(5 * time.Minute)
	}
}

func ConvertCurrency(from string, fromAmount float64, to string) float64 {
	if from != "USD" && to == "USD" {
		return fromAmount / exchangeRates[from]
	}
	if from != "USD" {
		return fromAmount / exchangeRates[from] * exchangeRates[to]
	}
	return fromAmount * exchangeRates[to]
}

func getExchangeRates() {
	rates := ExchangeRateData{}
	resp, err := http.Get("https://freecurrencyapi.net/api/v2/latest?apikey=7d2ba410-699f-11ec-9a7d-2502969b5ca3")
	if err != nil {

	}
	defer resp.Body.Close()
	json.NewDecoder(resp.Body).Decode(&rates)
	v := reflect.ValueOf(rates.Data)
	typeOfS := v.Type()
	for i := 0; i < v.NumField(); i++ {
		rate := v.Field(i).Interface().(float64)
		currency := strings.ToUpper(typeOfS.Field(i).Name)
		exchangeRates[currency] = rate
		log.Println(currency + " : " + fmt.Sprintf("%f", rate))
	}
}

func (a *App) convertCurrency(w http.ResponseWriter, r *http.Request) error {
	params := mux.Vars(r)
	currency := params["currency"]
	currencSplit := strings.Split(currency, "-")
	f, err := strconv.ParseFloat(currencSplit[0], 8)
	if err != nil {

		return err
	}
	convertedAmount := ConvertCurrency(currencSplit[1], f, currencSplit[2])
	_, err = w.Write([]byte(fmt.Sprintf("%f", convertedAmount)))
	return nil
}

func (a *App) exchange(w http.ResponseWriter, r *http.Request) error {
	if r.Method == http.MethodGet {
		token := ParseJwtToken(r)
		wallets := a.Repo.GetUserWallets(token.UserId)
		html := "<option name=\"\" value=\"\" selected disabled hidden>Select Wallet</option>"
		for _, wallet := range wallets {
			html += " <option  name=\"" + wallet.Currency + "\" = value=\"" + strconv.Itoa(int(wallet.ID)) + "\">" + wallet.Currency + ":" + strconv.Itoa(int(wallet.Balance)) + "</option>"
		}
		err := a.ParsedTemplates.ExecuteTemplate(w, "exchange-money.html", models.HtmlOptions{
			Html: template.HTML(html),
		})
		if err != nil {
			return err
		}
	} else if r.Method == http.MethodPost {
		err := r.ParseForm()
		if err != nil {
			return err
		}
		selectWalletId := r.PostForm.Get("selectWallet")
		sendAmountStr := r.PostForm.Get("sendAmount")
		receiveAmountStr := r.PostForm.Get("receiveAmount")
		receiveCurrency := r.PostForm.Get("receiveCurrency")

		parsedIdFrom, err := strconv.ParseUint(selectWalletId, 10, 32)
		if err != nil {
			return err
		}
		senderWallet, err := a.Repo.GetWalletById(uint(parsedIdFrom))
		if err != nil {
			return err
		}
		sendAmount, err := strconv.ParseFloat(sendAmountStr, 32)
		if err != nil {
			return err
		}
		receiverWallet, err := a.Repo.GetUserWalletByCurrency(senderWallet.Owner.ID, receiveCurrency)
		if err != nil || receiverWallet.ID == 0 {
			log.Println("User had no wallet in " + receiveCurrency + " , going to create a new one")
			receiverWallet, err = a.Repo.CreateWallet(senderWallet.Owner.ID, receiveCurrency)
			if err != nil {
				return err
			}
		}
		receiveAmount, err := strconv.ParseFloat(receiveAmountStr, 32)
		if err != nil {
			return err
		}

		if senderWallet.Balance < sendAmount {
			_, err := w.Write([]byte("Not enough balance!"))
			if err != nil {
				LogError(err)
				return err
			}
		}
		_, err = a.Repo.CreateTransaction(senderWallet, receiverWallet, receiveAmount, sendAmount, models.Exchange)
		if err != nil {
			log.Println("!!!!!!!!! Exchange transaction went kaput!")
		}
		data := models.SuccessTemplate{
			Action:     "Exchanged Money",
			ActionVerb: "exchanged ",
			Amount:     senderWallet.Currency + " " + sendAmountStr + " to " + receiverWallet.Currency + " " + receiveAmountStr,
		}
		err = a.ParsedTemplates.ExecuteTemplate(w, "deposit-money-success.html", data)
	}
	return nil
}

type ExchangeRateData struct {
	Query struct {
		Apikey       string `json:"apikey"`
		Timestamp    int    `json:"timestamp"`
		BaseCurrency string `json:"base_currency"`
	} `json:"query"`
	Data struct {
		Jpy float64 `json:"JPY"`
		Cny float64 `json:"CNY"`
		Chf float64 `json:"CHF"`
		Cad float64 `json:"CAD"`
		Mxn float64 `json:"MXN"`
		Inr float64 `json:"INR"`
		Brl float64 `json:"BRL"`
		Rub float64 `json:"RUB"`
		Krw float64 `json:"KRW"`
		Idr float64 `json:"IDR"`
		Try float64 `json:"TRY"`
		Sar float64 `json:"SAR"`
		Sek float64 `json:"SEK"`
		Ngn float64 `json:"NGN"`
		Pln float64 `json:"PLN"`
		Ars float64 `json:"ARS"`
		Nok float64 `json:"NOK"`
		Twd float64 `json:"TWD"`
		Irr float64 `json:"IRR"`
		Aed float64 `json:"AED"`
		Cop float64 `json:"COP"`
		Thb float64 `json:"THB"`
		Zar float64 `json:"ZAR"`
		Dkk float64 `json:"DKK"`
		Myr float64 `json:"MYR"`
		Sgd float64 `json:"SGD"`
		Ils float64 `json:"ILS"`
		Hkd float64 `json:"HKD"`
		Egp float64 `json:"EGP"`
		Php float64 `json:"PHP"`
		Clp float64 `json:"CLP"`
		Pkr float64 `json:"PKR"`
		Iqd float64 `json:"IQD"`
		Dzd float64 `json:"DZD"`
		Kzt float64 `json:"KZT"`
		Qar float64 `json:"QAR"`
		Czk float64 `json:"CZK"`
		Pen float64 `json:"PEN"`
		Ron float64 `json:"RON"`
		Vnd float64 `json:"VND"`
		Bdt float64 `json:"BDT"`
		Huf float64 `json:"HUF"`
		Uah float64 `json:"UAH"`
		Aoa float64 `json:"AOA"`
		Mad float64 `json:"MAD"`
		Omr float64 `json:"OMR"`
		Cuc float64 `json:"CUC"`
		Byr float64 `json:"BYR"`
		Azn float64 `json:"AZN"`
		Lkr float64 `json:"LKR"`
		Sdg float64 `json:"SDG"`
		Syp float64 `json:"SYP"`
		Mmk float64 `json:"MMK"`
		Dop float64 `json:"DOP"`
		Uzs float64 `json:"UZS"`
		Kes float64 `json:"KES"`
		Gtq float64 `json:"GTQ"`
		Ury float64 `json:"URY"`
		Hrv float64 `json:"HRV"`
		Mop float64 `json:"MOP"`
		Etb float64 `json:"ETB"`
		Crc float64 `json:"CRC"`
		Tzs float64 `json:"TZS"`
		Tmt float64 `json:"TMT"`
		Tnd float64 `json:"TND"`
		Pab float64 `json:"PAB"`
		Lbp float64 `json:"LBP"`
		Rsd float64 `json:"RSD"`
		Lyd float64 `json:"LYD"`
		Ghs float64 `json:"GHS"`
		Yer float64 `json:"YER"`
		Bob float64 `json:"BOB"`
		Bhd float64 `json:"BHD"`
		Cdf float64 `json:"CDF"`
		Pyg float64 `json:"PYG"`
		Ugx float64 `json:"UGX"`
		Svc float64 `json:"SVC"`
		Ttd float64 `json:"TTD"`
		Afn float64 `json:"AFN"`
		Npr float64 `json:"NPR"`
		Hnl float64 `json:"HNL"`
		Bih float64 `json:"BIH"`
		Bnd float64 `json:"BND"`
		Isk float64 `json:"ISK"`
		Khr float64 `json:"KHR"`
		Gel float64 `json:"GEL"`
		Mzn float64 `json:"MZN"`
		Bwp float64 `json:"BWP"`
		Pgk float64 `json:"PGK"`
		Jmd float64 `json:"JMD"`
		Xaf float64 `json:"XAF"`
		Nad float64 `json:"NAD"`
		All float64 `json:"ALL"`
		Ssp float64 `json:"SSP"`
		Mur float64 `json:"MUR"`
		Mnt float64 `json:"MNT"`
		Nio float64 `json:"NIO"`
		Lak float64 `json:"LAK"`
		Mkd float64 `json:"MKD"`
		Amd float64 `json:"AMD"`
		Mga float64 `json:"MGA"`
		Xpf float64 `json:"XPF"`
		Tjs float64 `json:"TJS"`
		Htg float64 `json:"HTG"`
		Bsd float64 `json:"BSD"`
		Mdl float64 `json:"MDL"`
		Rwf float64 `json:"RWF"`
		Kgs float64 `json:"KGS"`
		Gnf float64 `json:"GNF"`
		Srd float64 `json:"SRD"`
		Sll float64 `json:"SLL"`
		Xof float64 `json:"XOF"`
		Mwk float64 `json:"MWK"`
		Fjd float64 `json:"FJD"`
		Ern float64 `json:"ERN"`
		Szl float64 `json:"SZL"`
		Gyd float64 `json:"GYD"`
		Bif float64 `json:"BIF"`
		Kyd float64 `json:"KYD"`
		Mvr float64 `json:"MVR"`
		Lsl float64 `json:"LSL"`
		Lrd float64 `json:"LRD"`
		Cve float64 `json:"CVE"`
		Djf float64 `json:"DJF"`
		Scr float64 `json:"SCR"`
		Sos float64 `json:"SOS"`
		Gmd float64 `json:"GMD"`
		Kmf float64 `json:"KMF"`
		Std float64 `json:"STD"`
		Xrp float64 `json:"XRP"`
		Aud float64 `json:"AUD"`
		Bgn float64 `json:"BGN"`
		Btc float64 `json:"BTC"`
		Jod float64 `json:"JOD"`
		Gbp float64 `json:"GBP"`
		Eth float64 `json:"ETH"`
		Eur float64 `json:"EUR"`
		Ltc float64 `json:"LTC"`
		Nzd float64 `json:"NZD"`
	} `json:"data"`
}
