package services

import (
	"VirtualWalletGo/models"
	"fmt"
	"html/template"

	"log"
	"net/http"
	"strconv"
	"strings"
)

func (a *App) loadCards(w http.ResponseWriter, r *http.Request) error {
	if r.Method == http.MethodGet {
		token := ParseJwtToken(r)
		user, err := a.Repo.GetUserById(token.UserId)
		if err != nil {
			return err
		}
		pic := user.Picture
		if pic == "" {
			pic = "images/resizegopher.jpg" //default
		}
		cards := a.Repo.GetUserCards(user.ID)
		cardsHtml := ""
		for _, card := range cards {
			cardImg := ""
			if card.Type == "Visa" {
				cardImg = "src=\"images/payment/visa.png\" alt=\"visa\""
			} else {
				cardImg = "src=\"images/payment/mastercard.png\" alt=\"mastercard\""
			}
			month := strconv.Itoa(card.ExpiryMonth)
			year := strconv.Itoa(card.ExpiryYear)
			cardsHtml += "  <div class=\"col-12 col-sm-6 col-lg-4\">           " +
				"     <div class=\"account-card account-card-primary text-white rounded p-3 mb-4 mb-lg-0\">        " +
				"          <p class=\"text-4\">XXXX-XXXX-XXXX-" + card.Number[len(card.Number)-4:] + "</p>           " +
				"       <p class=\"d-flex align-items-center\"> <span class=\"account-card-expire text-uppercase d-inline-block opacity-7 mr-2\">Valid<br>   " +
				"                 thru<br>                    </span> <span class=\"text-4 opacity-9\">" + month + "/" + year + "</span> <span class=\"badge badge-warning text-0 font-weight-500 rounded-pill px-2 ml-auto\">Primary</span> </p>     " +
				"             <p class=\"d-flex align-items-center m-0\"> <span class=\"text-uppercase font-weight-500\">" + card.Owner.FirstName + " " + card.Owner.LastName + "</span> <img class=\"ml-auto\" " + cardImg + " title=\"\"> </p>    " +
				"              <div class=\"account-card-overlay rounded\"> <a href=\"#\" data-target=\"#edit-card-details\" data-toggle=\"modal\" class=\"text-light btn-link mx-2\"><span class=\"mr-1\"><i class=\"fas fa-edit\"></i></span>Edit</a>" +
				" <a href=\"#\" class=\"text-light btn-link mx-2\"><span class=\"mr-1\"><i class=\"fas fa-minus-circle\"></i></span>Delete</a> </div>          " +
				"      </div>              </div>"
		}
		walletsHtml := ""
		var total float64
		for _, wallet := range user.Wallets {
			if wallet.Currency == "USD" {
				total += wallet.Balance
			} else {
				total += ConvertCurrency(wallet.Currency, wallet.Balance, "USD")
			}
			walletsHtml += "      <div class=\"col-5 col-sm-5\">" +
				"     <div class=\"account-card account-card-primary text-white rounded mb-4 mb-lg-0\">" +
				"                                <div class=\"row no-gutters\">" +
				"                                    <div class=\"col-3 d-flex justify-content-center p-3\">" +
				"                                        <div class=\"my-auto text-center\"><span class=\"text-13\"><i" +
				"                                                class=\"fas fa-wallet\"></i></span>" +
				"                                        </div>" +
				"                                    </div>" +
				"                                    <div class=\"col-9 border-left\">" +
				"                                        <div class=\"py-4 my-2 pl-4\">" +
				"                                            <p class=\"text-4 font-weight-500 mb-1\">" + wallet.Currency + "</p>" +
				"                                            <p class=\"text-4 opacity-9 mb-1\">$" + strconv.Itoa(int(wallet.Balance)) + "</p>" +
				"                                            <p class=\"m-0\">Approved <span class=\"text-3\"><i" +
				"                                                    class=\"fas fa-check-circle\"></i></span></p>" +
				"                                        </div>" +
				"                                    </div>" +
				"                                </div>" +
				"                            </div>" +
				"                          </div>"

		}
		model := models.UserDashboard{
			Name:        user.FirstName + " " + user.LastName,
			Balance:     fmt.Sprintf("%.2f", total),
			CardAdded:   false,
			BankAdded:   false,
			Picture:     pic,
			RecentTx:    nil,
			CardsHtml:   template.HTML(cardsHtml),
			WalletsHtml: template.HTML(walletsHtml),
		}
		err = a.ParsedTemplates.ExecuteTemplate(w, "settings-payment-methods.html", model)
		if err != nil {
			return err
		}
	}
	return nil
}

func (a *App) addCard(w http.ResponseWriter, r *http.Request) error {
	token := ParseJwtToken(r)
	err := r.ParseForm()
	if err != nil {
		http.Error(w, "Please pass the data as URL form encoded", http.StatusBadRequest)
		return err
	}
	cardType := r.PostForm.Get("options")
	cardIssuer := r.PostForm.Get("cardType")
	cardNumber := r.PostForm.Get("cardnumber")
	if cardNumber != "" {
		cardNumber = strings.ReplaceAll(cardNumber, " ", "")
		if len(cardNumber) != 16 {
			return NewHTTPError(err, http.StatusBadRequest, "Card number should be 16 digits!")
		}
	} else {
		return NewHTTPError(err, http.StatusBadRequest, "Please fill out card number!")
	}

	expiryDate := r.PostForm.Get("expiryDate")
	if !strings.Contains(expiryDate, "/") || len(expiryDate) != 5 {
		return NewHTTPError(err, http.StatusBadRequest, "Please input expiry date in DD/MM format!")
	}
	dates := strings.Split(expiryDate, "/")
	month, _ := strconv.Atoi(dates[0])
	year, _ := strconv.Atoi(dates[1])
	cvv := r.PostForm.Get("cvvNumber")
	cvvInt, _ := strconv.Atoi(cvv)

	if err != nil {
		return err
	}

	card := models.CardInfo{
		UserID:      token.UserId,
		Number:      cardNumber,
		Type:        cardType,
		Brand:       cardIssuer,
		CvvNum:      cvvInt,
		ExpiryMonth: month,
		ExpiryYear:  year,
		IsDeleted:   false,
	}
	a.Repo.SaveCard(&card)
	if err != nil {
		log.Println("!!! Could not save Card")
		respondWithError(w, http.StatusInternalServerError, "!!! Could not save Card")
		return err
	}
	log.Println("Finished saving card..")
	http.Redirect(w, r, "/auth/paymentmethods", http.StatusFound)
	return nil
}

func sumBalance(wallets []models.Wallet) float64 {
	var balance float64 = 0.0
	for _, wallet := range wallets {
		if wallet.Currency == "USD" {
			balance += wallet.Balance
		} else {
			balance += ConvertCurrency(wallet.Currency, wallet.Balance, "USD")
		}
	}
	return balance
}
