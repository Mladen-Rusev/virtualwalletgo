package services

import (
	"VirtualWalletGo/models"
	"github.com/gorilla/mux"
	"html/template"
	"log"
	"net/http"
	"net/smtp"
	"strconv"
	"strings"
	"time"
)

func (a *App) verifyTransaction(w http.ResponseWriter, r *http.Request) error {
	params := mux.Vars(r)
	txIdStr := params["id"]
	txId, err := strconv.Atoi(txIdStr)
	if err != nil {
		return err
	}
	tx := a.Repo.GetTransactionById(uint(txId))
	if (tx.SettledAt != time.Time{}) {
		log.Println("!!!! This transaction has already been verified!")
		http.Redirect(w, r, "http://localhost:8080/auth/transactions", http.StatusFound)
		return err
	}
	senderWallet := tx.SenderWallet
	receiverWallet := tx.ReceiverWallet
	senderWallet.Balance -= tx.Amount
	receiverWallet.Balance += tx.Amount
	tx.SettledAt = time.Now()
	tx.Status = models.Settled

	err = a.Repo.SaveTransaction(tx)
	err = a.Repo.SaveWallet(&senderWallet)
	err = a.Repo.SaveWallet(&receiverWallet)
	if err != nil {
		return err
	}
	data := models.SuccessTemplate{
		Action:     "Send Money",
		ActionVerb: "sent ",
		Amount:     senderWallet.Currency + " " + strconv.Itoa(int(tx.Amount)),
	}
	err = a.ParsedTemplates.ExecuteTemplate(w, "deposit-money-success.html", data)
	return err
}

func (a *App) loadTransactions(w http.ResponseWriter, r *http.Request) error {
	token := ParseJwtToken(r)
	if r.Method == http.MethodGet {
		model, user, err := generateUserData(a.Repo, token.UserId)
		transactions := a.Repo.FilterUserTransactions(token.UserId)
		html, _ := generateTransactionDtos(user, transactions)
		model.CardsHtml = template.HTML(html)
		err = a.ParsedTemplates.ExecuteTemplate(w, "transactions.html", model)
		if err != nil {
			return err
		}
	} else {
		err := r.ParseForm()
		date := strings.Split(r.PostForm.Get("dateRange"), " - ")
		dateFrom, err := time.Parse("01/02/2006", date[0])
		if err != nil {
			return err
		}
		dateTo, err := time.Parse("01/02/2006", date[1])
		filter := r.PostForm.Get("allFilters")
		model, user, err := generateUserData(a.Repo, token.UserId)
		transactions := a.Repo.FilterUserTransactionsFilters(token.UserId, dateFrom, dateTo, filter)
		html, _ := generateTransactionDtos(user, transactions)
		model.CardsHtml = template.HTML(html)
		model.Picture = user.Picture
		err = a.ParsedTemplates.ExecuteTemplate(w, "transactions.html", model)
		if err != nil {
			return err
		}
	}
	return nil
}

func sendVerifEmail(email string, amount string, id uint) {
	from := "ridepal.mladen.stefan@gmail.com"
	password := "mladenstefan."
	to := []string{email}
	smtpHost := "smtp.gmail.com"
	smtpPort := "587"

	message := "Click the link to verify your transaction for " + amount + ". " + "link<http://localhost:8080/verify/" + strconv.Itoa(int(id)) + ">"
	msg := []byte("From: " + from + "\n" +
		"To: " + to[0] + "\n" +
		"Subject: Hello there\n\n" +
		message)
	//todo CORRECT LINK IF HOST APP!!
	auth := smtp.PlainAuth("", from, password, smtpHost)

	// Send actual message
	err := smtp.SendMail(smtpHost+":"+smtpPort, auth, from, to, msg)
	log.Println("Mail should have been sent? maybe >.>")
	LogError(err)
}

func generateTransactionDtos(user *models.User, transactions []models.Transaction) (string, float64) {
	html := ""
	for _, transaction := range transactions {
		day := strconv.Itoa(transaction.CreatedAt.Day())
		month := transaction.CreatedAt.Month().String()[:3]
		entity := transaction.ToName
		txType := transaction.Type.String()
		currency := transaction.Currency

		amount := "+"
		if txType == "Send" && user.ID == transaction.ReceiverUserID {
			txType = "Receive" + " from " + transaction.FromName
		} else if txType == "Send" {
			txType += " to " + transaction.ToName
			amount = "-"
		} else if txType == "Exchange" {
			currency = transaction.ReceiverWallet.Currency
			entity = "Exchange " + transaction.SenderWallet.Currency + " to " + transaction.ReceiverWallet.Currency
		}
		amount += strconv.Itoa(int(transaction.Amount))

		tooltip := "<span class=\"text-success\" data-toggle=\"tooltip\" data-original-title=\"Completed\"><i class=\"fas fa-check-circle\"></i>"
		if transaction.Status == models.Pending {
			tooltip = "<span class=\"text-warning\" data-toggle=\"tooltip\" data-original-title=\"In Progress\"><i class=\"fas fa-ellipsis-h\"></i>"
		}

		html += " <div class=\"transaction-item px-4 py-3\">\n  " +
			"     <div class=\"row align-items-center flex-row\">\n" +
			"        <div class=\"col-2 col-sm-1 text-center\"> <span class=\"d-block text-4 font-weight-300\">" + day + "</span> <span class=\"d-block text-1 font-weight-300 text-uppercase\">" + month + "</span> </div>\n" +
			"           <div class=\"col col-sm-7\"> <span class=\"d-block text-4\">" + entity + "</span> <span class=\"text-muted\">" + txType + "</span> </div>\n" +
			"            <div class=\"col-auto col-sm-2 d-none d-sm-block text-center text-3\">" + tooltip + "</span> </div>\n" +
			"           <div class=\"col-3 col-sm-2 text-right text-4\"> <span class=\"text-nowrap\">" + amount + "</span> <span class=\"text-2 text-uppercase\">(" + currency + ")</span> </div>\n" +
			"        </div>\n" +
			"  </div>"
	}
	return html, 0
}
