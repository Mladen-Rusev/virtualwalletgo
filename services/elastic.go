package services

import (
	"VirtualWalletGo/models"
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"github.com/elastic/go-elasticsearch/v5"
	"github.com/elastic/go-elasticsearch/v5/esapi"
	"log"
	"regexp"
	"strconv"
	"strings"
)

//Docker image was running with 6.3 gigs
//Tokenizing emails
func (a *App) InitElasticSearch() {
	var r map[string]interface{}

	es, err := elasticsearch.NewDefaultClient()
	if err != nil {
		log.Fatalf("Error creating the client: %s", err)
	}

	// 1. Get cluster info
	//
	res, err := es.Info()
	if err != nil {
		log.Fatalf("Error getting response: %s", err)
	}
	defer res.Body.Close()
	// Check response status
	if res.IsError() {
		log.Fatalf("Error: %s", res.String())
	}
	// Deserialize the response into a map.
	if err := json.NewDecoder(res.Body).Decode(&r); err != nil {
		log.Fatalf("Error parsing the response body: %s", err)
	}
	// Print client and server version numbers.
	log.Printf("Client: %s", elasticsearch.Version)
	log.Printf("Server: %s", r["version"].(map[string]interface{})["number"])
	log.Println(strings.Repeat("~", 37))
	a.Elastic = es

	// 2. Index documents
	users, err := a.Repo.GetAllUsers()
	if err != nil {
		LogError(err)
	}

	for _, user := range users {
		a.indexUser(int(user.ID), &user)
	}
	log.Println(strings.Repeat("-", 37))
}

func (a *App) indexUser(i int, user *models.User) {
	email := user.Email
	email = strings.Split(email, "@")[0]
	re, err := regexp.Compile("[^a-zA-Z0-9 -]")
	str1 := re.ReplaceAllString(email, " ")
	user.IndexEmail = str1
	user.IndexName = user.FirstName + " " + user.LastName
	user.Wallets = nil
	log.Println(str1)
	// Build the request body.
	var buf bytes.Buffer
	err = json.NewEncoder(&buf).Encode(user)
	if err != nil {

	}

	// Set up the request object.
	req := esapi.IndexRequest{
		Index:      "test",
		DocumentID: strconv.Itoa(i + 1),
		Body:       &buf,
		Refresh:    "true",
	}

	// Perform the request with the client.
	res, err := req.Do(context.Background(), a.Elastic)
	if err != nil {
		log.Fatalf("Error getting response: %s", err)
	}
	defer res.Body.Close()

	if res.IsError() {
		log.Printf("[%s] Error indexing document ID=%d", res.Status(), i+1)
	} else {
		// Deserialize the response into a map.
		var r map[string]interface{}
		if err := json.NewDecoder(res.Body).Decode(&r); err != nil {
			log.Printf("Error parsing the response body: %s", err)
		} else {
			// Print the response status and indexed document version.
			log.Printf("[%s] %s; version=%d", res.Status(), r["result"], int(r["_version"].(float64)))
		}
	}
}

func (a *App) emailSearch(searchInput string) []models.User {
	// 3. Search for the indexed documents
	//
	// Build the request body.
	es := a.Elastic
	var buf bytes.Buffer
	query := map[string]interface{}{
		"query": map[string]interface{}{
			"match": map[string]interface{}{
				"IndexEmail": map[string]interface{}{
					"query":     searchInput,
					"fuzziness": 2,
				},
			},
		},
	}

	if err := json.NewEncoder(&buf).Encode(query); err != nil {
		log.Fatalf("Error encoding query: %s", err)
	}

	// Perform the search request.
	res, err := es.Search(
		es.Search.WithContext(context.Background()),
		es.Search.WithIndex("test"),
		es.Search.WithBody(&buf),
		es.Search.WithPretty(),
	)
	if err != nil {
		log.Fatalf("Error getting response: %s", err)
	}
	defer res.Body.Close()

	if res.IsError() {
		var e map[string]interface{}
		if err := json.NewDecoder(res.Body).Decode(&e); err != nil {
			log.Fatalf("Error parsing the response body: %s", err)
		} else {
			// Print the response status and error information.
			log.Fatalf("[%s] %s: %s",
				res.Status(),
				e["error"].(map[string]interface{})["type"],
				e["error"].(map[string]interface{})["reason"],
			)
		}
	}
	var r map[string]interface{}
	if err := json.NewDecoder(res.Body).Decode(&r); err != nil {
		log.Fatalf("Error parsing the response body: %s", err)
	}
	// Print the response status, number of results, and request duration.
	log.Printf(
		"[%s] %d hits; took: %dms",
		res.Status(),
		int(r["hits"].(map[string]interface{})["total"].(map[string]interface{})["value"].(float64)),
		int(r["took"].(float64)),
	)
	// Print the ID and document source for each hit.
	//var user models.User
	users := []models.User{}
	for _, hit := range r["hits"].(map[string]interface{})["hits"].([]interface{}) {
		jsonString, _ := json.Marshal(hit.(map[string]interface{})["_source"])
		fmt.Println(string(jsonString))
		// convert json to struct
		user := models.User{}
		json.Unmarshal(jsonString, &user)
		users = append(users, user)
		if err != nil {

		}
		log.Printf(" * ID=%s, %s", hit.(map[string]interface{})["_id"], hit.(map[string]interface{})["_source"])
	}

	log.Println(strings.Repeat("=", 37))
	return users
}

func (a *App) nameSearch(searchInput string) []models.User {
	// 3. Search for the indexed documents
	//
	// Build the request body.
	es := a.Elastic
	var buf bytes.Buffer
	query := map[string]interface{}{
		"query": map[string]interface{}{
			"match": map[string]interface{}{
				"IndexName": map[string]interface{}{
					"query":     searchInput,
					"fuzziness": 2,
				},
			},
		},
	}

	if err := json.NewEncoder(&buf).Encode(query); err != nil {
		log.Fatalf("Error encoding query: %s", err)
	}

	// Perform the search request.
	res, err := es.Search(
		es.Search.WithContext(context.Background()),
		es.Search.WithIndex("test"),
		es.Search.WithBody(&buf),
		es.Search.WithPretty(),
	)
	if err != nil {
		log.Fatalf("Error getting response: %s", err)
	}
	defer res.Body.Close()

	if res.IsError() {
		var e map[string]interface{}
		if err := json.NewDecoder(res.Body).Decode(&e); err != nil {
			log.Fatalf("Error parsing the response body: %s", err)
		} else {
			// Print the response status and error information.
			log.Fatalf("[%s] %s: %s",
				res.Status(),
				e["error"].(map[string]interface{})["type"],
				e["error"].(map[string]interface{})["reason"],
			)
		}
	}
	var r map[string]interface{}
	if err := json.NewDecoder(res.Body).Decode(&r); err != nil {
		log.Fatalf("Error parsing the response body: %s", err)
	}
	// Print the response status, number of results, and request duration.
	log.Printf(
		"[%s] %d hits; took: %dms",
		res.Status(),
		int(r["hits"].(map[string]interface{})["total"].(map[string]interface{})["value"].(float64)),
		int(r["took"].(float64)),
	)
	// Print the ID and document source for each hit.
	users := []models.User{}
	for _, hit := range r["hits"].(map[string]interface{})["hits"].([]interface{}) {
		jsonString, _ := json.Marshal(hit.(map[string]interface{})["_source"])
		fmt.Println(string(jsonString))
		// convert json to struct
		user := models.User{}
		json.Unmarshal(jsonString, &user)
		users = append(users, user)
		if err != nil {

		}
		log.Printf(" * ID=%s, %s", hit.(map[string]interface{})["_id"], hit.(map[string]interface{})["_source"])
	}

	log.Println(strings.Repeat("=", 37))
	return users
}
