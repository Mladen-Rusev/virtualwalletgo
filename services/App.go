package services

import (
	"VirtualWalletGo/dao"
	"VirtualWalletGo/models"
	"context"
	"github.com/cloudinary/cloudinary-go"
	"github.com/dgrijalva/jwt-go"
	"github.com/elastic/go-elasticsearch/v5"
	"github.com/go-errors/errors"
	"github.com/gorilla/mux"
	"html/template"
	"log"
	"net/http"
	"strings"
)

var SECRET = "ALL_YOUR_BASE_ARE_BELONG_TO_US"

const BEARER_SCHEMA = "jwtTokenCookie="

type App struct {
	Router          *mux.Router
	Repo            *dao.RepoGormMysql
	ParsedTemplates *template.Template
	Cloudinary      *cloudinary.Cloudinary
	Server          *http.Server
	Elastic         *elasticsearch.Client
}

func (a *App) Init(user, password, dbname string) {
	a.Repo = dao.NewUserRepoMysql(user, password, dbname)
	cld, err := cloudinary.NewFromParams("dmksxmopa", "465741858995892", "Y5h5AKGZpS9qUFgsk7c-HnCOU9w")
	if err != nil {

	}
	a.Cloudinary = cld
	go LaunchExchangeRatesService()
	go a.InitElasticSearch()
	a.ParsedTemplates, err = template.ParseGlob("template/*.html")
	if err != nil {
		log.Fatalln(err)
	}
	a.Router = mux.NewRouter()
	a.initializeRoutes()
}

func (a *App) Run(addr string) {
	a.Server = &http.Server{
		Addr:    addr,
		Handler: a.Router,
	}
	go func() {
		log.Println("Starting HTTP server on", addr)
		log.Println("Press Ctrl+C to stop the server ...")
		if err := a.Server.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("Error server listen: %v\n", err)
		}
	}()
}

func (a *App) Shutdown(ctx context.Context) error {
	return a.Server.Shutdown(ctx)
}

type rootHandler func(http.ResponseWriter, *http.Request) error

func (fn rootHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	err := fn(w, r) // Call handler function
	if err == nil {
		return
	}
	LogError(err)
	clientError, ok := err.(ClientError) // Check if it is a ClientError.
	if !ok {
		// If the error is not ClientError, assume that it is ServerError.
		w.WriteHeader(500) // return 500 Internal Server Error.
		return
	}

	body, err := clientError.ResponseBody() // Try to get response body of ClientError.
	if err != nil {
		log.Printf("An error accured: %v", err)
		w.WriteHeader(500)
		return
	}
	status, headers := clientError.ResponseHeaders() // Get http status code and headers.
	for k, v := range headers {
		w.Header().Set(k, v)
	}
	w.WriteHeader(status)
	w.Write(body)
}

func (a *App) initializeRoutes() {
	defaultRouter := a.Router
	defaultRouter.PathPrefix("/vendor/").Handler(http.FileServer(http.Dir("./template/")))
	defaultRouter.PathPrefix("/css/").Handler(http.FileServer(http.Dir("./template/")))
	defaultRouter.PathPrefix("/images/").Handler(http.FileServer(http.Dir("./template/")))
	defaultRouter.PathPrefix("/js/").Handler(http.FileServer(http.Dir("./template/")))

	defaultRouter.Handle("/", rootHandler(a.index))
	defaultRouter.Handle("/login", rootHandler(a.login))
	defaultRouter.Handle("/logout", rootHandler(a.logout))
	defaultRouter.Handle("/register", rootHandler(a.registerUser))
	defaultRouter.Handle("/checkPhone/{email}", rootHandler(a.checkPhone))      //add api prefix
	defaultRouter.Handle("/convert/{currency}", rootHandler(a.convertCurrency)) //add api as prefix;add version
	defaultRouter.Handle("/verify/{id}", rootHandler(a.verifyTransaction))      //add api as prefix;add version
	defaultRouter.Handle("/searchmail/{s}", rootHandler(a.searchMail))
	defaultRouter.Handle("/searchname/{s}", rootHandler(a.searchName))

	// Auth route
	authRouter := defaultRouter.PathPrefix("/auth").Subrouter()
	authRouter.PathPrefix("/auth/").Handler(http.StripPrefix("/auth/", http.FileServer(http.Dir("./template/"))))
	defaultRouter.PathPrefix("/auth/vendor/").Handler(http.StripPrefix("/auth", http.FileServer(http.Dir("./template/"))))
	defaultRouter.PathPrefix("/auth/css/").Handler(http.StripPrefix("/auth", http.FileServer(http.Dir("./template/"))))
	defaultRouter.PathPrefix("/auth/images/").Handler(http.StripPrefix("/auth", http.FileServer(http.Dir("./template/"))))
	defaultRouter.PathPrefix("/auth/js/").Handler(http.StripPrefix("/auth", http.FileServer(http.Dir("./template/"))))
	authRouter.Use(JwtVerify)

	authRouter.Handle("/profile", rootHandler(a.profile))
	authRouter.Handle("/uploadPic", rootHandler(a.uploadPic))
	authRouter.Handle("/changeUserDetails", rootHandler(a.changeUserDetails))
	authRouter.Handle("/paymentmethods", rootHandler(a.loadCards))
	authRouter.Handle("/addcard", rootHandler(a.addCard))
	authRouter.Handle("/send", rootHandler(a.loadSend))
	authRouter.Handle("/deposit", rootHandler(a.deposit))
	authRouter.Handle("/postDeposit", rootHandler(a.postDeposit))
	authRouter.Handle("/exchange", rootHandler(a.exchange))
	authRouter.Handle("/transactions", rootHandler(a.loadTransactions))

	defaultRouter.PathPrefix("/verify/vendor/").Handler(http.StripPrefix("/verify", http.FileServer(http.Dir("./template/"))))
	defaultRouter.PathPrefix("/verify/css/").Handler(http.StripPrefix("/verify", http.FileServer(http.Dir("./template/"))))
	defaultRouter.PathPrefix("/verify/images/").Handler(http.StripPrefix("/verify", http.FileServer(http.Dir("./template/"))))
	defaultRouter.PathPrefix("/verify/js/").Handler(http.StripPrefix("/verify", http.FileServer(http.Dir("./template/"))))
}

func JwtVerify(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		//var header = r.Header.Get("x-access-token") //Grab the token from the header
		var header = r.Header.Get("Cookie") //Grab the token from the header

		var token string
		if header == "" || len(header) <= len(BEARER_SCHEMA) {
			http.Redirect(w, r, " http://localhost:8080/login", http.StatusFound)
			return
		}
		if strings.HasPrefix(header, "REVEL_FLASH=;") {
			header = header[len("REVEL_FLASH=;")+1:]
		}
		token = header[len(BEARER_SCHEMA):]
		token = strings.TrimSpace(token)

		if token == "" {
			//Token is missing, returns with error code 401 Unauthorized
			log.Println("TOken = empty ")
			http.Redirect(w, r, " http://localhost:8080/login", http.StatusFound)
			return
		}
		claims := &models.UserToken{}

		_, err := jwt.ParseWithClaims(token, claims, func(token *jwt.Token) (interface{}, error) {
			return []byte(SECRET), nil
		})

		if err != nil {
			respondWithError(w, http.StatusUnauthorized, err.Error())
			return
		}
		ctx := context.WithValue(r.Context(), "UserToken", claims)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

func LogError(err error) {
	if err, ok := err.(stackTracer); ok {
		trace := err.StackTrace()
		log.Println(trace.String())
	}
}

type stackTracer interface {
	StackTrace() errors.StackFrame
}
