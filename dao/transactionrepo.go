package dao

import (
	"VirtualWalletGo/models"
	"gorm.io/gorm"
	"log"
	"time"
)

func (r RepoGormMysql) FilterUserTransactionsFilters(userId uint, from time.Time, to time.Time, txTypeString string) []models.Transaction {
	var transactions = []models.Transaction{}
	if txTypeString != "all" {
		txType := models.TransactionTypeFromString(txTypeString)
		if txType == models.Send {
			tx := r.db.Where("sender_user_id = ? and (created_at > ? and created_at < ?) and type = ?", userId, from, to, txType).Find(&transactions)
			if tx.Error != nil {
				log.Println(tx.Error)
			}
		} else {
			tx := r.db.Where(" receiver_user_id = ? and (created_at > ? and created_at < ?) and type = ?", userId, from, to, txType).Find(&transactions)
			if tx.Error != nil {
				log.Println(tx.Error)
			}
		}
	} else {
		tx := r.db.Where("(sender_user_id = ? or receiver_user_id = ?) and (created_at > ? and created_at < ?)", userId, userId, from, to).Find(&transactions)
		if tx.Error != nil {
			log.Println(tx.Error)
		}
	}
	return transactions
}

func (r RepoGormMysql) FilterUserTransactions(userId uint) []models.Transaction {
	var transactions = []models.Transaction{}
	tx := r.db.Preload("SenderWallet").Preload("ReceiverWallet").Where("sender_user_id = ? or receiver_user_id = ?", userId, userId).Find(&transactions)
	if tx.Error != nil {
		log.Println(tx.Error)
	}
	return transactions
}

func (r RepoGormMysql) GetTransactionById(txId uint) *models.Transaction {
	var transaction = models.Transaction{}
	tx := r.db.Preload("SenderWallet").Preload("ReceiverWallet").Where("id = ?", txId).Find(&transaction)
	if tx.Error != nil {
		log.Println(tx.Error)
	}
	return &transaction
}

func (r RepoGormMysql) SaveTransaction(tx *models.Transaction) error {
	if err := r.db.Save(tx).Error; err != nil {
		// return any error will rollback
		return err
	}
	return nil
}

func (r RepoGormMysql) CreateTransaction(fromWallet *models.Wallet, toWallet *models.Wallet, addAmount float64, removeAmount float64, txType models.TransactionType) (models.Transaction, error) {
	res := models.Transaction{}
	err := r.db.Transaction(func(tx *gorm.DB) error {
		// do some database operations in the transaction (use 'tx' from this point, not 'db')
		pending := removeAmount >= 10000 && txType == models.Send
		transaction := models.Transaction{
			Model:            gorm.Model{},
			ToName:           toWallet.Owner.FirstName + " " + toWallet.Owner.LastName,
			ReceiverUserID:   toWallet.UserID,
			ReceiverWalletID: toWallet.ID,
			ReceiverWallet:   *toWallet,
			Amount:           addAmount,
			Fee:              0,
			Type:             txType,
			Status:           models.Settled,
		}
		if fromWallet != nil {
			transaction.FromName = fromWallet.Owner.FirstName + " " + fromWallet.Owner.LastName
			transaction.SenderUserID = fromWallet.UserID
			transaction.SenderWalletID = fromWallet.ID
			transaction.SenderWallet = *fromWallet
			transaction.Currency = fromWallet.Currency
		} else {
			//case when it a deposit we have no from wallet
			transaction.Currency = toWallet.Currency
		}
		if pending {
			transaction.Status = models.Pending
		} else {
			if fromWallet != nil {
				fromWallet.Balance -= removeAmount
				if err := tx.Save(fromWallet).Error; err != nil {
					return err
				}
			}
			toWallet.Balance += addAmount
			transaction.SettledAt = time.Now()
		}
		// return any error will rollback
		if err := tx.Save(&transaction).Error; err != nil {
			// return any error will rollback
			return err
		}
		if err := tx.Save(toWallet).Error; err != nil {
			return err
		}
		res = transaction
		// return nil will commit the whole transaction
		return nil
	})
	return res, err
}
