package dao

import (
	"VirtualWalletGo/models"
	"gorm.io/gorm"
)

func (r RepoGormMysql) GetUserByMail(email string) (*models.User, error) {
	user := &models.User{}
	var tx *gorm.DB
	tx = r.db.Preload("Wallets").Where("email = ?", email).First(&user)
	return user, tx.Error
}

func (r RepoGormMysql) GetUserByPhone(phone string) (*models.User, error) {
	user := &models.User{}
	var tx *gorm.DB
	tx = r.db.Preload("Wallets").Where("phone = ?", phone).First(&user)
	return user, tx.Error
}

func (r RepoGormMysql) GetUserById(id uint) (*models.User, error) {
	user := &models.User{}
	tx := r.db.Preload("Wallets").Where("id = ?", id).First(&user)
	return user, tx.Error
}

func (r RepoGormMysql) GetAllUsers() ([]models.User, error) {
	users := []models.User{}
	tx := r.db.Preload("Wallets").Find(&users)
	return users, tx.Error
}

func (r RepoGormMysql) RegisterUser(user *models.User) error {
	tx := r.db.Create(user)
	return tx.Error
}

func (r RepoGormMysql) SaveUser(user *models.User) error {
	tx := r.db.Save(user)
	return tx.Error
}
