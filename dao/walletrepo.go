package dao

import (
	"VirtualWalletGo/models"
	"gorm.io/gorm"
	"log"
)

func (r RepoGormMysql) CreateWallet(userId uint, currency string) (*models.Wallet, error) {
	if currency == "" {
		currency = "BGN" //default
	}
	wallet := models.Wallet{
		Model:    gorm.Model{},
		UserID:   userId,
		Currency: currency,
		Balance:  0,
		IsActive: true,
	}
	tx := r.db.Create(&wallet)
	if tx.Error != nil {
		tx.Rollback()
	}
	return &wallet, nil
}

func (r RepoGormMysql) GetUserWallets(userId uint) []models.Wallet {
	var wallets = make([]models.Wallet, 0)
	tx := r.db.Preload("Owner").Where("user_id = ?", userId).Find(&wallets)
	if tx.Error != nil {
		log.Println(tx.Error)
	}
	return wallets
}

func (r RepoGormMysql) SaveWallet(wallet *models.Wallet) error {
	tx := r.db.Updates(&wallet)
	if tx.Error != nil {
		return tx.Error
	}
	return nil
}

func (r RepoGormMysql) GetWalletById(id uint) (*models.Wallet, error) {
	wallet := models.Wallet{}
	tx := r.db.Preload("Owner").Where("id = ?", id).Find(&wallet)
	if tx.Error != nil {
		return nil, tx.Error
	}
	return &wallet, nil
}

func (r RepoGormMysql) GetUserWalletByCurrency(userId uint, currency string) (*models.Wallet, error) {
	wallet := models.Wallet{}
	tx := r.db.Preload("Owner").Where("user_id = ?", userId).Where("currency = ?", currency).Find(&wallet)
	if tx.Error != nil {
		return nil, tx.Error
	}
	if wallet.Currency == "" {
		return nil, nil
	}
	return &wallet, nil

}
