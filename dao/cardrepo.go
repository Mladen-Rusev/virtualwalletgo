package dao

import (
	"VirtualWalletGo/models"
	"log"
)

func (r RepoGormMysql) SaveCard(card *models.CardInfo) {
	tx := r.db.Create(card)
	if tx.Error != nil {
		tx.Rollback()
		log.Println("!!!!! ERROR in SaveCard.. rollback")
	}
	tx.Commit()
}

func (r RepoGormMysql) GetUserCards(id uint) []models.CardInfo {
	cards := make([]models.CardInfo, 0)
	tx := r.db.Joins("Owner").Where("user_id = ?", id).Find(&cards)
	if tx.Error != nil {
		log.Println("!!!! COuld not retrieve cards for user: " + (string(id)))
		return nil
	}
	return cards
}
