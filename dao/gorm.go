package dao

import (
	"VirtualWalletGo/models"
	"fmt"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"log"
	"os"
	"time"
)

type RepoGormMysql struct {
	db *gorm.DB
}

func NewUserRepoMysql(user, password, dbname string) *RepoGormMysql {
	connectionString := fmt.Sprintf("%s:%s@tcp(127.0.0.1:3306)/%s?parseTime=true", user, password, dbname)
	repo := &RepoGormMysql{}
	var err error
	newLogger := logger.New(
		log.New(os.Stdout, "\r\n", log.LstdFlags), // io writer
		logger.Config{
			SlowThreshold:             time.Second, // Slow SQL threshold
			LogLevel:                  logger.Info, // Log level
			IgnoreRecordNotFoundError: true,        // Ignore ErrRecordNotFound error for logger
			Colorful:                  false,       // Disable color
		},
	)
	repo.db, err = gorm.Open(mysql.Open(connectionString), &gorm.Config{
		DisableForeignKeyConstraintWhenMigrating: true,
		Logger:                                   newLogger,
	})
	if err != nil {
		log.Fatal(err)
	}
	err = repo.db.AutoMigrate(&models.User{})
	err = repo.db.AutoMigrate(&models.Wallet{})
	err = repo.db.AutoMigrate(&models.Transaction{})
	err = repo.db.AutoMigrate(&models.CardInfo{})
	if err != nil {
		log.Fatal("Couldnt build db tables")
	}
	//golangci-lint
	return repo
}
