# Project Title
-=Payyed=-

A web application written in Golang as the final project submission for Chaos Group Golang Training 2021-2022 (Sofia)

This web app allows users to register an account, create a payment method (input card details), top up their accounts through a created payment method, convert currency, send currency to other users of the app and manage profile info. Wallets with different currencies within an account are supported.

Cloudinary API was used for image storage and management.

FreeCurrencyApi for live currency exchange rates.

Elasticsearch was included as proof of concept - currently indexes Users' emails and names  (see https://gitlab.com/Mladen-Rusev/virtualwalletgo/-/blob/main/services/elastic.go)

![alt text](https://res.cloudinary.com/dmksxmopa/image/upload/v1642970843/payd/app1_ghnhd4.png "Title Text")
![alt text](https://res.cloudinary.com/dmksxmopa/image/upload/v1642970843/payd/app2_gkpeo9.png "Title Text")
![alt text](https://res.cloudinary.com/dmksxmopa/image/upload/v1642970843/payd/app4_zdvm1l.png "Title Text")
![alt text](https://res.cloudinary.com/dmksxmopa/image/upload/v1642970843/payd/app3_ssxuo3.png "Title Text")

### Prerequisites

If you want to run the project locally you will need: Docker compose

### Installing
Download the docker compose file https://gitlab.com/Mladen-Rusev/virtualwalletgo/-/blob/main/docker-compose.yml

Navigate to download folder and run docker compose up.

This will run 3 containers - mysql on port 3306 , elastic on port 9200, and the app on port 8080. 

## Built With
* [Golang]
* [GorillaMux]
* [Mysql]
* [ElasticSearch]
* [Docker]

## Author
* **Mladen Rusev** 
