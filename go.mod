module VirtualWalletGo

go 1.17

require (
	github.com/cloudinary/cloudinary-go v1.5.0
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-errors/errors v1.4.1
	github.com/gorilla/mux v1.8.0
	golang.org/x/crypto v0.0.0-20211215153901-e495a2d5b3d3
	gorm.io/driver/mysql v1.2.1
	gorm.io/gorm v1.22.4
)

require (
	github.com/creasty/defaults v1.5.1 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/go-sql-driver/mysql v1.6.0 // indirect
	github.com/google/uuid v1.3.0 // indirect
	github.com/gorilla/schema v1.2.0 // indirect
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/jinzhu/now v1.1.3 // indirect
	github.com/stretchr/testify v1.7.0 // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
)
