package models

import "strings"

type TransactionType int64

const (
	Send     TransactionType = iota
	Deposit                  = 3
	Exchange                 = 4
)

func (s TransactionType) String() string {
	switch s {
	case Send:
		return "Send"
	case Deposit:
		return "Deposit"
	case Exchange:
		return "Exchange"
	}
	return "opa"
}

func TransactionTypeFromString(typeStr string) TransactionType {
	switch strings.ToLower(typeStr) {
	case "send":
		return Send
	case "deposit":
		return Deposit
	case "exchange":
		return Exchange
	}
	return Send
}

type TransactionStatus int64

const (
	Pending TransactionStatus = iota
	Settled
	Failed
)

func (s TransactionStatus) String() string {
	switch s {
	case Pending:
		return "Pending"
	case Settled:
		return "Settled"
	case Failed:
		return "Failed"
	}
	return "opa"
}
