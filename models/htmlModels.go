package models

import (
	"html/template"
)

type UserDashboard struct {
	Name        string
	Dob         string
	Email       string
	Phone       string
	Balance     string
	CardAdded   bool
	BankAdded   bool
	Picture     string
	RecentTx    []TransactionDTO
	CardsHtml   template.HTML
	WalletsHtml template.HTML
}

type TransactionDTO struct {
	Date     string
	From     string
	To       string
	Type     string
	Status   string
	Amount   string
	Currency string
}

type CardDto struct {
	LastFourDigits string
	ValidDates     string
	OwnerName      string
	CardType       string
}

type HtmlOptions struct {
	Html template.HTML
}

type SuccessTemplate struct {
	Action     string
	ActionVerb string
	Amount     string
}
