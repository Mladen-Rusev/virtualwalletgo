package models

import (
	"github.com/dgrijalva/jwt-go"
	"gorm.io/gorm"
	"time"
)

type User struct {
	gorm.Model
	FirstName  string
	LastName   string
	Password   string
	Phone      string `gorm:"index:idx_member;unique"`
	Email      string `gorm:"index:idx_member;unique"`
	IndexEmail string `gorm:"-"`
	IndexName  string `gorm:"-"`
	Picture    string
	Dob        time.Time
	HasCard    bool
	CardInfo   []CardInfo
	Wallets    []Wallet
	isAdmin    bool
}

type Wallet struct {
	gorm.Model
	UserID   uint
	Owner    User `gorm:"foreignKey:UserID;references:ID"`
	Currency string
	Balance  float64
	IsActive bool
}

type Transaction struct {
	gorm.Model
	SenderUserID      uint
	SenderWalletID    uint
	SenderWallet      Wallet `gorm:"foreignKey:SenderWalletID;references:ID"`
	ReceiverUserID    uint
	ReceiverWalletID  uint
	ReceiverWallet    Wallet `gorm:"foreignKey:ReceiverWalletID;references:ID"`
	FromName          string
	ToName            string
	SettledAt         time.Time
	Status            TransactionStatus
	Type              TransactionType
	Amount            float64
	Fee               float64
	Currency          string
	ConvertedCurrency string
}

type CardInfo struct {
	gorm.Model
	UserID      uint
	Owner       User `gorm:"foreignKey:UserID;references:ID"`
	Number      string
	Type        string
	Brand       string
	CvvNum      int
	ExpiryMonth int
	ExpiryYear  int
	IsDeleted   bool
}

type UserToken struct {
	UserId uint   `json:"userId"`
	Name   string `json:"name"`
	Email  string `json:"email"`
	jwt.StandardClaims
}
